<?php
	try {
		$bdd = new PDO('mysql:host=localhost;dbname=chat;charset=utf8', 'root', 'root');
	} catch (Exception $e) {
		exit('Erreur de connexion à la base de données.');
	}


	//if($bdd->exec('CREATE DATABASE IF NOT EXISTS chat') {
  //$sql = "CREATE TABLE `chat`.`messages` (nom VARCHAR(255), message TEXT, dates TEXT)";
  //$bdd->exec($sql);
	//}

	 $me = "/me ";
	 $clear = "/clear";
	 $ban = "/ban ";
   $space = " ";
	 $name = htmlspecialchars($_POST['name']);
	 $message = htmlspecialchars($_POST['message']);
   $date = date("F j, Y, g:i a");
	 $posClear = strpos($message, $clear);
	 $posMe = strpos($message, $me);
   $posBan = strpos($message, $ban);
   $posName = strpos($message, $space, 5);
	if(isset($_POST['ok'])){
		if(!empty($_POST['name']) AND (strlen($_POST['message']) > 0)){
		  if ($posMe === false) {
			$insertmsg = $bdd->prepare("INSERT INTO messages(nom, message, dates) VALUES(?, ?, ?)");
			$insertmsg -> execute(array($name, $message, $date));
		  }
		  else {
			  if($posMe == 0){
				$message = "<span class='grey'>" . substr_replace($message, '', $posMe , $posMe+3) . "</span>";
				$insertmsg = $bdd->prepare("INSERT INTO messages(nom, message, dates) VALUES(?, ?, ?)");
				$insertmsg -> execute(array($name, $message, $date));
			  }
			  else if ($posMe != 0 AND $posBan === false){
				$error = "Attention : Le '$me' doit être obligatoirement au début du message !";
			  }
			  else {
				 $error = "Erreur : Veuillez modifier le message";
			  }
			}
		}
		else{
			if($posBan == 0 && $posClear === false){
			$banName = substr($message, $posBan+4, $posName-4);
			$banName = trim($banName, " ");
			$banReason = substr($message, $posName);
			$banReason = "<span class='red'> Censuré par la modération parce que :" . $banReason . "</span>";

			$reqname = $bdd->prepare("SELECT * FROM messages WHERE nom = ?");
			$reqname->execute(array($banName));
			$nameExist = $reqname->rowCount();
			if($nameExist >= 1 && $posClear === false){
				$editmsg = $bdd->prepare("UPDATE messages SET message = ? WHERE nom = ? ");
				$editmsg -> execute(array($banReason, $banName));
			}
			else if ($nameExist == 0){
				$error = "Utilisateur inexistant !";
			}
			}
			else if($posBan != 0 && $posClear === false){
			 $error = "Attention : Le '$Ban' doit être obligatoirement au début du message !";
			}
			else if($posClear === false && $posClear === false){
		  	$error = "Tous les champs ne sont pas rempli !";
			}
			else {
				$sql = "DELETE FROM messages";
		    $bdd->exec($sql);
			}
		}
	}

	include('template_index.php');
?>
