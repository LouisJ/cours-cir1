<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Chat</title>
  <link rel="stylesheet" href="style.css">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link rel="stylesheet" href="https://bootswatch.com/4/cosmo/bootstrap.min.css">
  <script src="script.js"></script>
</head>
<body>
  <?php
      $allmsg = $bdd->query('SELECT * FROM messages');
      while($msg = $allmsg->fetch()){
        echo "<b>" . $msg['nom'] . "</b> : " . $msg['message'] . "</br>";
      }
 ?>
  <form method="post" action="">
    <table>
      <tr>
        <td><label for="name">Name :</label></td>
        <td><input type="text" name="name" size="10" maxlength="20" value="<?php echo $name ?>" /></td>
        <td><label for="message">Message :</label></td>
        <td><textarea id="message" name="message"></textarea></td>
      </tr>
	 </table>
      <div class="boutton"> <input type="submit" name="ok" value="Submit" /> </div>
  </form>

    <?php echo '<b><p>' . $error . "</p></b><br>";?>
</body>
</html>
