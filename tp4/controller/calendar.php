<?php
include('../model/model.php');
function calendar($bdd, $month, $year){
	$number_of_day = cal_days_in_month(CAL_GREGORIAN, $month, $year); //Retourne le nombre de jours dans un mois, pour une année et un calendrier donné
	echo '<div class="calendar"><table class="table table-hover">'; //On crée le tableau
	echo "<tr><th>Lun</th><th>Mar</th><th>Mer</th><th>Jeu</th><th>Ven</th><th>Sam</th><th>Dim</th></tr>";//On affiche les jours
	for ($i=1; $i <= $number_of_day ; $i++) {
		$day = cal_to_jd(CAL_GREGORIAN, $month, $i, $year); //Convertion du calendrier en nombre de jours du Calendrier Gregorien
		$day_week = jddayOfWeek($day); //Numéro du jour de la semaine
		$date = date_create($year.'-'.$month.'-'.$i); //On affiche la date dans le bon format
		$date = date_format($date, 'Y-m-d H:i:s'); //On affiche la date dans le bon format avec l'heure
		$event = getEvent($bdd, $date); //On récupére les évenement du jour
		$type = 0; //On définie la limite d'affichage à 5)
		if ($i == $number_of_day){//Si c'est le premier jour du mois
			if ($day_week == 1){ //Si c'est le premier jour de la semaine
				echo "<tr>";
			}
			echo "<td class='case'>";
			ifEnvent($event, $date, $type);
			echo $i ."</td></tr>";
		}
    else if ($i == 1){ //Pour le premier jour du mois ..
			echo "<tr>";
			if ($day_week == 0){
				$day_week = 7;
			}
			for ($k=1; $k != $day_week ; $k++){
				echo "<td></td>";
			}
			echo "<td class='case'>";
			ifEnvent($event, $date, $type);
			echo $i ."</td>";
			if ($day_week == 7){
				echo "</tr>";
			}
		}
    else{
			if ($day_week == 1){
				echo "<tr>";
			}
			echo "<td class='case'>";
			ifEnvent($event, $date, $type);
			echo $i ."</td>";
			if ($day_week == 0) {
				echo "</tr>";
			}
		}
	}
	echo "</table></div>";
} //Fonction affichage du calendrier

if (isset($_SESSION['id']) AND isset($_SESSION['login'])){
  if(isset($_POST['ok']) && $_SESSION['rank'] == "ORGANIZER"){
      if(!empty($_POST['nameEvent']) && !empty($_POST['beginDate']) && !empty($_POST['endDate']) && !empty($_POST['numberPlace'])){
				$insertevent = $bdd->prepare("INSERT INTO events(name, description, startdate, enddate, organizer_id, nb_place) VALUES(?, ?, ?, ?, ?, ?)");
				$insertevent -> execute(array($_POST['nameEvent'], $_POST['decription'], $_POST['beginDate'], $_POST['endDate'], $_SESSION['id'], $_POST['numberPlace']));
				$success = "Evénement '". $_POST['nameEvent'] ."' ajouté !";
      }
      else{
				$error = "Veuillez renseigner tous les champs !";
      }
    }
} //Ajout événement

include('../view/calendar_template.php');

?>
