<?php
include('../model/model.php');
if(isset($_GET['id'])){ //Récupération données événements
  $reqevent = $bdd->prepare("SELECT * FROM events WHERE id = ?");
  $reqevent->execute(array($_GET['id']));
  $event = $reqevent->fetch(); //Recherche info de l'évenement
  $_SESSION['idDeleteEvent'] = $_GET['id']; //Mise en variable globales de l'id de l'évenement si on veut supprimer
  $_SESSION['idEvent'] = $event['id']; // ID Event
  $_SESSION['place'] = $event['nb_place']; //Places restantes
  $reqevent = $bdd->prepare("SELECT * FROM user_participates_events WHERE id_participant = ? AND id_event = ?");
  $reqevent->execute(array($_SESSION['id'], $_SESSION['idEvent']));
  $user = $reqevent->rowCount(); //Combien de fois l'utilisateur est inscrit : 1 ou 0.
}

if(isset($_POST['deleteEvent'])){ //Suppréssion si demande de l'organisateur d'un événement
	$reqevent = $bdd->prepare("DELETE FROM events WHERE id = ?");
	$reqevent->execute(array($_SESSION['idDeleteEvent']));
	$error = "Evenement Supprimé !";
  header("Location: calendar.php");

}//Si on appuie pour supprimer un événement : On le supprime

if(isset($_POST['come'])){
		$_SESSION['place'] -=1;
		$reqevent = $bdd->prepare("UPDATE events SET nb_place = ? WHERE id = ?");
		$reqevent->execute(array($_SESSION['place'], $_SESSION['idEvent']));
		$reqevent = $bdd->prepare("INSERT INTO user_participates_events(id_participant, id_event) VALUES(?, ?)");
		$reqevent->execute(array($_SESSION['id'], $_SESSION['idEvent']));
		$success = "Inscription effectué !";
    header("Location: calendar.php");
} //Si on appuie pour venir à un événement : On ajoute l'utilisateur et on enleve une place

if(isset($_POST['leave'])){
		$_SESSION['place'] +=1;
		$reqevent = $bdd->prepare("UPDATE events SET nb_place = ? WHERE id = ?");
		$reqevent->execute(array($_SESSION['place'], $_SESSION['idEvent']));
		$reqevent = $bdd->prepare("DELETE FROM user_participates_events WHERE id_participant = ? AND id_event = ?");
		$reqevent->execute(array($_SESSION['id'], $_SESSION['idEvent']));
		$success = "Inscription annulé !";
    header("Location: calendar.php");
} //Si on appuie pour ne plus venir à un événement : On enleve l'utilisateur et on rajoute une place


include('../view/evenement_template.php');
?>
