<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Connexion</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</head>
<body>
  <div class="container">
    <h1>TP4 : Calendar</h1>
    <form method="post" action="">
      <div class="form-row">
        <div class="form-group col-md-6">
          <label for="inputEmail4">Login</label>
          <input type="text" class="form-control" id="inputEmail4" placeholder="Login" name="login">
          </div>
          <div class="form-group col-md-6">
            <label for="inputPassword4">Password</label>
            <input type="password" class="form-control" id="inputPassword4" placeholder="Password" name="password">
          </div>
        </div>
      <button type="submit" class="btn btn-secondary btn-lg" name="ok">Submit</button>
    </form></br>

    <?php
      if(strlen($error) > 0){
        echo '<div class="alert alert-danger" role="alert">'. htmlspecialchars($error) .'</div>';
      }
      else if(strlen($success) > 0){
        echo '<div class="alert alert-success" role="alert">'. htmlspecialchars($success) .'</div>';
      }
    ?>
  </div>
</body>
</html>
