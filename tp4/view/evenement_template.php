<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Evenement</title>
  <link rel="stylesheet" href="../view/style.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body class="container">
  <h1><?php echo htmlspecialchars($event['name']); ?> </h1>
  <?php
  echo "<div class='event'><b>Description : </b>" . htmlspecialchars($event['description']) . "</br>";
  echo "<b>Début : </b>" . htmlspecialchars($event['startdate']) . "</br>";
  echo "<b>Fin : </b>" . htmlspecialchars($event['enddate']) . "</br>";
  echo "<b>Place : </b>" . htmlspecialchars($event['nb_place']) . "</br></div>";


  if($_SESSION['rank'] == 'ORGANIZER'){
    echo "<form method='post' action=''><input type='submit' value='Annuler' name='deleteEvent'></form></br>";
  }
  else if($user == 0){
    echo "<form method='post' action=''><input type='submit' value='Participer' name='come' class='btn btn-primary'></form></br>";
  }
  else if($user >= 1){
    $success = "Incrit";
    echo "<form method='post' action=''><button type='submit' name='leave' class='btn btn-danger'>Ne plus venir</button></form></br>";
  }

  echo "</br><form method='post' action='calendar.php'><button class='btn btn-info'>Retour</button></form></br>";

  if(strlen($error) > 0){
    echo '<div class="alert alert-danger" role="alert">'. htmlspecialchars($error) .'</div>';
  }
  else if(strlen($success) > 0){
    echo '<div class="alert alert-info" role="alert">'. htmlspecialchars($success) .'</div>';
  }
?>

</body>
</html>
