<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Organisateur</title>
  <link rel="stylesheet" href="../view/style.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body class="container">
  <h1 style="text-align:center;">
    <?php echo month($month)." ".$year; ?>
  </h1>
  <div class="btn-group" role="group" aria-label="Basic example" style="text-align:center;">
    <form method="post" action="calendar.php">
    <button type="input" class="btn btn-secondary" name="before"> <- </button>
    </form>
    <form method="post" action="calendar.php">
    <button type="input" class="btn btn-secondary" name="next">-></button>
    </form>
  </div>

  <div>
  	<?php calendar($bdd, $month, $year);?>
  </div>
  </br></br>
  <?php if($_SESSION['rank'] =='ORGANIZER'){?>
  <div class="addEvent">
    <form method="post" action="">
          <label for="nameEvent">Nom de l'event :</label>
          <input type="text" name="nameEvent" size="20" maxlength="20"/>
          <label for="decription">Description :</label>
          <input type="text" name="decription" id="decription" size="30"/>
          <label for="beginDate">Début :</label>
          <input type="date" name="beginDate" id="beginDate"/>
          <label for="endDate">Fin :</label>
          <input type="date" name="endDate" id="endDate"/>
          <label for="numberPlace">Place :</label>
          <input type="number" name="numberPlace" id="numberPlace" size="30"/>
        </tr>
        <tr><td><input type="submit" name="ok" value="Submit"/></td></tr>
     </table>
    </form>
   </div>
 <?php }?>
   <form method="post" action="calendar.php">
     <input type="submit" value="Déconnection" name="logout" class='btn btn-danger' >
   </form></br>
   <?php
     if(strlen($error) > 0){
       echo '<div class="alert alert-danger" role="alert">'. htmlspecialchars($error) .'</div>';
     }
     else if(strlen($success) > 0){
       echo '<div class="alert alert-success" role="alert">'. htmlspecialchars($success) .'</div>';
     }
   ?>
</body>
</html>
