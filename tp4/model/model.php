<?php session_start();
$error = "";
$success = "";
$month = $_SESSION['month'];
$day = $_SESSION['day'];
$year = $_SESSION['year'];

try {
  $bdd = new PDO('mysql:host=localhost;dbname=Users;charset=utf8', 'root', 'root');
} catch (Exception $e) {
  exit('Erreur de connexion à la base de données.' . $e -> getMessage());
} //Connection BDD
if(isset($_POST['before'])){
	$_SESSION['month']--;
	if($_SESSION['month'] == 0){
		$_SESSION['month'] = 12;
		$_SESSION['year']--;
	}
	header("Location: ../controller/calendar.php");
} //Afficher pour le mois précedent

if(isset($_POST['next'])){
	$_SESSION['month']++;
	if($_SESSION['month']%13 == 0){
		$_SESSION['month'] = 1;
		$_SESSION['year']++;
	}
	header("Location: ../controller/calendar.php");
} //Afficher pour le mois suivant

if(isset($_POST['logout'])){
  session_destroy();
  $success = "Déconnection effectué";
  header("Location: ../index.php");
} //Déconnection

function month($month) {
	$month_ = $month-1;
	$monthLetter = array("Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre");
	return $monthLetter[$month_];
} //Fonction mois en toutes lettres

function getEvent($bdd, $date) {
  if($_SESSION['rank'] == 'ORGANIZER'){
    $reqevent = $bdd->prepare("SELECT * FROM events WHERE startdate <= ? AND enddate >= ? AND organizer_id = ?");
    $reqevent->execute(array($date, $date, $_SESSION['id']));
  }
  else {
    $reqevent = $bdd->prepare("SELECT * FROM events WHERE startdate <= ? AND enddate >= ?");
    $reqevent->execute(array($date, $date));
  }
  $event = $reqevent->fetchAll();
  return $event;
} //Récupération de tous les événements

function ifEnvent($event, $date, $type){ //Si l'événement existe affichage
  $g = 0;
  foreach($event as $row){
    if($g == 5 && $type == 0){
      break;
    }
    if($row[6]>0 || $_SESSION['rank'] == 'ORGANIZER'){
      echo "<form method='post' action='../controller/evenement.php?id=" . htmlspecialchars($row[0]) ."'><input type='submit' value='". htmlspecialchars($row[1]) ."'></form>"; //Affichage événement
      $g++;
    }
  }
  if($g >= 5 && $type == 0) {
    $_SESSION['event'] = $event;
    echo "</br><form method='post' action='../controller/allevent.php?date=". htmlspecialchars($date, ENT_QUOTE) ."'><input type='submit' value='Voir +'></form>"; //Si nombre d'évenement supérieur à 5 affichage bouton voir +
  }
}
?>
