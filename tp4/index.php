<?php
  include('model/model.php');
  $_SESSION['day'] = date('d');
  $_SESSION['month'] = date('m');
  $_SESSION['year'] = date('Y'); //Mise en variable session de la date d'aujourd'hui
  if(isset($_POST['ok'])){
  		if(!empty($_POST['login']) AND !empty($_POST['password'])){

        $requser = $bdd->prepare("SELECT * FROM Users WHERE login = ?");
  			$requser->execute(array($_POST['login']));
        $userinfo = $requser->fetch(); //Récupération des informations utilisateurs

  			if($userinfo != FALSE && password_verify($_POST['password'], $userinfo['password']) == TRUE){//Si login et MDP OK
          $_SESSION['id']=$userinfo['id'];
          $_SESSION['login']=$userinfo['login'];
          $_SESSION['rank']=$userinfo['rank']; //Mise en variables globales de toutes les informations de l'utilisateur
          header("Location: controller/calendar.php"); //Redirection -> Callendrier
        }
  			else {
  				$error = "Mauvais login/password !";
  			}
      }
  		else {
        $error = "Tous les champs ne sont pas rempli !";
      }

  	}
  include('view/index_template.php');
