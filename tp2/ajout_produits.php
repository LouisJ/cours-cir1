<?php
	session_start();
	$error = " ";
	if(isset($_POST['ok'])){
		$price = $_POST['price'];
		$quantity = $_POST['quantity'];
		$name = $_POST['name'];
		$limite = 1000;
			/////////////////////////////////////////////////////////////////////////////

			$folder = 'uploads/';
			$file = basename($_FILES['picture']['name']);
			$size_max = 100000;
			$size = filesize($_FILES['picture']['tmp_name']);
			$extensions = array('.png', '.gif', '.jpg', '.jpeg');
			$extension = strrchr($_FILES['picture']['name'], '.');



			if(!in_array($extension, $extensions))
			{
				 $errorIMG = 'Vous devez uploader un file de type png, gif, jpg, jpeg...';
			}
			if($size>$size_max)
			{
				 $errorIMG = 'Le file est trop gros...';
			}
			if(!isset($errorIMG))
			{
				$file = strtr($file,
					  'ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ',
					  'AAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy');
				 $file = preg_replace('/([^.a-z0-9]+)/i', '-', $file);
				 if(move_uploaded_file($_FILES['picture']['tmp_name'], $folder . $file))
				 {
					  echo 'Upload effectué avec succès !';
				 }
				 else
				 {
					  echo $errorIMG = 'Echec de l\'upload !';
				 }
			}

			///////////////////////////////////////////////////////////////////////////////

		$chemin = 'mesproduts.csv';
		$delimiteur = ';';
		$ligne = [$file, $name, $price, $quantity];
		$_SESSION['array'] = $ligne;
		if ($price > 0){
		  if (!preg_match("#^[a-z0-9A-Z]+$#", $name)){
			  $error = "Un nom ne doit pas contenir autre chose que des lettres ou des chiffres";
		  }
		  else
		  {
			$file_csv = fopen("mesproduts.csv", "a+");
			fprintf($file_csv, chr(0xEF).chr(0xBB).chr(0xBF));
			fputcsv($file_csv, $ligne, $delimiteur);
			fclose($file_csv);
			header('Location: liste_produits.php');
		  }
		}
		else{
		  $error = "Le prix doit être supérieur à Zéro !";
    }
  }

	include('template_ajout_produits.html');
?>
